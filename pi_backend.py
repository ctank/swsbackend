""" pi_backend.py

This is a REST relay server running on the Pi for the Arduino to relay data 
to on the way to the Cloud. Effectively functions as a gateway for
the edge devices.

"""

from flask import Flask, request, render_template, Response
import client

# Create the flask app
app = Flask(__name__)

# This is for the callback, which should be defined to
# with the following signature:
# def callback(temp, humid, light)
#

_callback_ = None

# Create just a single route to read data from the Arduino
@app.route('/data', methods = ['GET'])
def addData():
    ''' The one and only route. It extracts the
    data from the request, converts to float if the
    data is not None, then calls the callback if it is set
    '''

    global _callback_

    temp = request.args.get('temp')
    humid = request.args.get('humid')
    light = request.args.get('light')

    if temp is not None:
        temp = float(temp)

    if humid is not None:
        humid = float(humid)

    if light is not None:
        light = float(light)

    if _callback_ is not None:
        _callback_(temp = temp, humid = humid, light = light)
    
    return "OK", 200


def set_callback(callback):
    ''' Sets the callback.

    @param callback The callback with signature def callback(temp, humid,
    light)

    '''

    global _callback_

    _callback_ = callback

def demo_callback(temp, humid, light):
    ''' Demo callback that just prints the data passed in.
    '''

    if temp is not None:
        print("\nTemperature = %3.2f" % temp)

    if humid is not None:
        print("Humidity = %3.2f" % humid)

    if light is not None:
        print("Light = %3.2f" % light)

    if temp is None and humid is None and light is None:
        print("\n*** Called without any parameters. ***")

    print("\n")

def main():
    set_callback(demo_callback)
    app.run(host = "0.0.0.0", port = '3237')


if __name__ == '__main__':
    main()
