'''! backend.py: Backend server code for SWS3009.

Requirements:
    MongoDB: See https://docs.mongodb.com/manual/installation/
    pymongo flask: pip3 install pymongo flask

To run locally:
    python backend

This creates a test server running on 0.0.0.0:3009 and will be accessible
to other hosts

'''


import pymongo
from pymongo import MongoClient, GEOSPHERE
from flask import Flask, request, render_template, Response
import hashlib
import codes
import datetime
import json
from bson.son import SON

# Global variables
mydb = None
users = None
data = None

PI = 3.141592654
def deg_to_rad(deg):
    '''! Converts degrees to radians. Dummy function does not do anything.
    '''
    return deg

##
# Response generation helper functions.
#

def make_result(result_code, result_details):
    '''! Create a result structure

    @param result_code: ERR_* codes as defined in codes.py
    @param result_details: String describing the outcome

    @return dict containing result and details.

    '''

    return {"result":result_code, "details":result_details}

def make_json_response(result, status):
    '''! Create a properly formed Flask Response object with the correct
    application/json MIME type.

    @param result   Python dictionary created by make_result or elsewhere.
    @param status   HTTP status, e.g. 200

    @return Flask Return class with mimetype set to application/json
    
    '''

    return Response(response = json.dumps(result),
    status = status,
    mimetype = "application/json")

##
# Create the app object and default '/' handler

app = Flask(__name__)

def find_remote_ip():
    print(request.headers)
    if 'X-Real-Ip' in request.headers:
        connect_ip = request.headers['X-Real-Ip']
    else:
        connect_ip = request.remote_addr

    return connect_ip

@app.route('/', methods = ['GET'])
def root():
    '''! Response for '/', prints a simple message.
    '''
    remote = find_remote_ip()

    return 'SWS3009 Backend Server. You are calling from host %s.' % (remote), 200

###########################
# User related endpoints
###########################


##
# User related helper functions
#

def find_user(userid):
    '''! Helper function: Search for a user in the database.

    @param userid   UserID to search for
    
    @return User's record or None if not found.
    
    '''

    query = {"userid":userid}

    if users.count_documents(query) > 0:
        return users.find_one(query)
    else:
        return None

# Check that the json has the userid and password fields

def check_json(in_json):
    '''! Helper function: Check if the JSON provided has the userid and passwrd
    fields.

    @param  in_json JSON to check on

    @return None if in_json works fine, Response object with ERR_BAD_REQUEST
    response otherwise.

    '''

    if in_json is None:
        return make_json_response(make_result(codes.ERR_BAD_REQUEST,
        "Bad JSON request."), 400)
    elif 'userid' not in in_json:
        return make_json_response(make_result(codes.ERR_BAD_REQUEST,
        "UserID missing."), 400)
    elif 'passwd' not in in_json:
        return make_json_response(make_result(codes.ERR_BAD_REQUEST,
        'Password missing.'), 400)
    else:
        return None

def check_user_pw(in_json):
    '''! Helper function: Check USER ID and Password. Uses check_json to ensure
    that the JSON has userid and password, then checks that the user exists,
    and then checks the password.

    @param in_json  JSON containing login details
    
    @return None if everything is ok, Response object otherwise with error
    message and code.

    '''

    ret = check_json(in_json)
    if ret is not None:
        return ret
    else:
        user_data = find_user(in_json['userid'])
        if user_data is None:
            return make_json_response(make_result(codes.ERR_BAD_REQUEST,
            "No such user %s." % in_json['userid']), 400)
        else:
            passwd_hash = hashlib.sha512(in_json['passwd'].encode('utf-8')).hexdigest()
            print("Stored hash: ", user_data["passwd"])
            print("Received hash: ", passwd_hash)

            if user_data['passwd'] != passwd_hash:
                return make_json_response(make_result(codes.ERR_BAD_REQUEST,
                "Bad current password."), 400)
            else:
                return None

def make_exception(e):
    return make_json_response(make_result(codes.ERR_UNKNOWN, str(e)), 400)

##
# User related routes
#

@app.route('/user/newuser', methods = ['POST'])
def addNewUser():
    ''' Add new user using /user/newuser

    JSON format:
        {'userid':<New user ID>, 'passwd': <New password>'}

        @return OK, 200 if successful. DUP_USER, 400 if user exists

    '''

    connect_ip = find_remote_ip()

    if connect_ip != '127.0.0.1':
        return 'Can only add new user from localhost', 403

    try:
    
        new_user = request.get_json();
        check_user = check_json(new_user)

        if check_user is None:
            if find_user(new_user["userid"]) is not None:
                return make_json_response(make_result(codes.ERR_DUP_USER, "User exists."),
                400)
            else:
                # Hash the password
                hash_pw = hashlib.sha512(new_user["passwd"].encode('utf-8')).hexdigest()
                new_user["passwd"] = hash_pw
                new_user["lastupload"] = datetime.datetime(1980, 1, 1)
                users.insert_one(new_user)
                return make_json_response(make_result(codes.ERR_OK, "OK"), 200)
        else:
            return check_user

    except Exception as e:
            return make_exception(e)

@app.route('/user/newpassword', methods = ['POST'])
def newPassword():
    ''' Change password using /user/newpassword route.

    JSON format:

    {'userid': <Existing user ID>, 'passwd': <Existing password>, 'new_passwd':
    <New password>}

    @return OK, 200 if OK, BAD_USER, 400 if user not found, BAD_PW,400 if
    existing password is bad, BAD_REQUEST,400 if new_passwd is missing.

    '''

    try:
        user = request.get_json()

        if user['userid'] == 'demo' or user['userid'] == 'admin':
            remote_ip = find_remote_ip()

            if remote_ip != '127.0.0.1':
                return make_json_response(make_result(codes.ERR_BAD_REQUEST,
                "Can only change demo and admin passwords from localhost."),
                400)
            
        check_user = check_user_pw(user)

        if check_user is None:
            if 'new_passwd' not in user:
                return make_json_response(make_result(codes.ERR_BAD_REQUEST,
                "New password is missing."), 400)
            else:
                hash_new_pw = hashlib.sha512(user["new_passwd"].encode('utf-8')).hexdigest()

                query = {"userid": user["userid"]}

                users.update_one(query, 
                {"$set":{"passwd":hash_new_pw}})

                return make_json_response(make_result(codes.ERR_OK, "OK"), 200)
        else:
            return check_user
    except Exception as e:
        return make_exception(e)

##
# Data related endpoints
#

@app.route('/data/add', methods = ['POST'])
def addData():
    ''' Add new data using the /data/add endpoint. Can only be called once
    every codes.UPLOAD_MIN_TIME seconds, nominally every 120 seconds.

    JSON format:
        Only 'userid', 'password', 'lat' and 'lon' are mandatory. The remaining
        fields may be omitted.

        {'userid': <User ID>, 'passwd': <Password>,
        'lat': <Latitude in degrees>, 'lon': <Longitude in degrees>,
        'timestamp': <Time stamp in dd:mm:yyyy:hh:mm:ss format>,
        'temp': <Temperature>, 'humid': <Humidity>,
        'light':<Light>, 'raw': <Binary data, must be base64 encoded>}

    @return OK, 200 if all ok, BAD_USER, 400 if userid does not exist, BAD_PW,
    400 if password provided is incorrect, BAD_TOO_FAST, 400 if called faster
    than once every codes. UPLOAD_MIN_TIME seconds, TOO_BIG, 400 if upload size
    exceeds codes.MAX_UPLOAD_SIZE bytes.

    Note: Set codes.ALLOW_NEW_FIELDS to True to allow arbitrary fields.
    Otherwise only the fields above will be allowed.
    
    '''

    if not codes.DATA_UPLOADS:
        return make_json_response(make_result(codes.ERR_READ_ONLY,
        "Server set to read-only. Cannot upload."), 400)

    try:
        newdata = request.get_json()
        check_pw = check_user_pw(newdata)

        if check_pw is None:
            # Get current time
            the_time = datetime.datetime.utcnow()
            
            # Compute difference between this and stored time.
            the_user = find_user(newdata['userid'])
            diff = the_time - the_user['lastupload']

            if diff.total_seconds() > codes.UPLOAD_MIN_TIME:
                # Check the size of the new object
                obj_size = len(newdata)

                insert_data = {"username":newdata["userid"]}

                if obj_size > codes.MAX_UPLOAD_SIZE:
                    return make_json_response(make_result(codes.ERR_TOO_BIG,
                    "Maximum upload size is %d bytes. You have %d bytes."
                    % (codes.MAX_UPLOAD_SIZE, objsize)), 400)
                else:
                    if 'lat' not in newdata or 'lon' not in newdata:
                        return
                        make_json_response(make_result(codes.ERR_BAD_REQUEST,
                        "Latitude/longitude data is missing."), 400)
                    else:
                        for field in newdata:
                            if codes.ALLOW_NEW_FIELDS is True:
                                insert_data[field] = newdata[field]
                            else:
                                if field in codes.FIELD_NAMES:
                                    insert_data[field] = newdata[field]
                        # Inser tthe data

                        if insert_data != {}:

                            # Add timestamp
                            if "timestamp" not in newdata:
                                insert_data["timestamp"] = the_time
                            else:
                                insert_data["timestamp"] = datetime.datetime.strptime(newdata["timestamp"],
                                codes.DATE_TIME_STR)

                            # Create geospatial field for query
                            lon_rad = deg_to_rad(newdata['lon'])
                            lat_rad = deg_to_rad(newdata['lat'])

                            insert_data['loc'] = [lon_rad, lat_rad]
                            print("LOC: ", insert_data['loc'])
                            data.insert_one(insert_data)


                            # Update the last uploaded time

                            query = {'userid':newdata['userid']}
                            users.update_one(query,
                            {'$set':{'lastupload':the_time}})

                            return make_json_response(make_result(codes.ERR_OK,
                            'OK'), 200)
                        else:
                            return
                            make_json_response(make_result(codes.ERR_EMPTY,
                            'NO DATA TO INSERT'), 400)

            else:
                return make_json_response(make_result(codes.ERR_TOO_FAST,
                "Minimum time between uploads is %d seconds."
                "Please wait another %d seconds before uploading again."
                % (codes.UPLOAD_MIN_TIME, codes.UPLOAD_MIN_TIME - diff.total_seconds())), 400)

        else:
            return check_pw

    except Exception as e:
        raise
        return make_exception(e)
## 
# Search helper functions
#

def make_query(query):
    ''' Helper function: Forms a proper query dictionary for the pymongo.find call.

    @param query    Query JSON, see searchData below for details

    @return {} if query is empty, otherwise a query dictionary to be used by
    pymongo.

    '''

    max_dist = None

    if 'lat' in query and 'lon' in query:
        print("Geo query.")
        # maxdist must be in meters
        if 'maxdist' in query:
            max_dist = query['maxdist']
        else:
            max_dist = codes.MAX_GPS_RADIUS

    timediff = None

    start_time = None
    end_time = None

    if 'timestamp' in query:
        # maxtimediff must be in minutes
        if 'maxtimediff' in query:
            timediff = query['maxtimediff']
        else:
            timediff = codes.MAX_TIME_DIFF
        end_time = datetime.datetime.strptime(query['timestamp'],
        codes.DATE_TIME_STR)

        start_time = end_time - datetime.timedelta(minutes = timediff)
        print("Start time: %s, end time: %s." %
        (datetime.datetime.strftime(start_time, codes.DATE_TIME_STR),
        datetime.datetime.strftime(end_time, codes.DATE_TIME_STR)))


    username = None

    if 'username' in query:
        username = query['username']

    userfield = {}
    timefield = {}
    geofield = {}

    if username is not None:
        userfield = {'username':username}

    if timediff is not None:
        timefield = {'timestamp':{'$gte': start_time, '$lte': end_time}}

    if max_dist is not None:
        lon_rad = deg_to_rad(query['lon'])
        lat_rad = deg_to_rad(query['lat'])

        geofield = {'loc':{'$nearSphere': SON([('$geometry',
        SON([('type', 'Point'), ('coordinates', [lon_rad,
        lat_rad])])), ('$maxDistance', max_dist)])}}

    if userfield == {} and timefield == {} and geofield == {}:
        return {}
    else:
        return {'$and':[userfield, timefield, geofield]}

def find_query(query, sortcode):
    '''! Helper function: Searches database for query. This function calls
    make_query first to generate the MongoDB query, then calls find to get the
    data.

    @param  query   Search query.
    @param  sortcode    Sorting code, see codes.py 

    @return count and mongoDB iterator object. count = 0 if nothing found.

    '''

    db_query = make_query(query)

    ''' This block of code does not work. count_documents is
    not compatible with $nearSphere.

    count = data.count_documents(db_query)
    if count > 0:
        return count, data.find(db_query, {'_id':0})
    else:
        return 0, None
    '''

    # Find sort options
    field = None
    order = None

    if sortcode == codes.SORT_USERID_ASC:
        field = 'username'
        order = pymongo.ASCENDING
    elif sortcode == codes.SORT_TIMESTAMP_ASC:
        field = 'timestamp'
        order = pymongo.ASCENDING
    elif sortcode == codes.SORT_USERID_DESC:
        field = 'username'
        order = pymongo.DESCENDING
    elif sortcode == codes.SORT_TIMESTAMP_DESC:
        field = 'timestamp'
        order = pymongo.DESCENDING
        
    # .count is deprecated but works with $nearSphere

    if field is None:
        ret = data.find(db_query, {'_id':0})
    else:
        ret = data.find(db_query, {'_id':0}).sort(field, order)

    return ret.count(), ret

@app.route('/data/search', methods = ['POST'])
def searchData():
    ''' Handles data seach using the /data/search endpoint.

    JSON format:

    All fields except userid and passwd are optional. If no fields are
    specified then all records are returned.

    {'userid': <User ID>, 'passwd': <Password>,
    'lat': <Latitude to search>, 'lon': Longitude to search,
    'maxdist': <Maximum distance in meters from [lon, lat] to search,
    'timestamp': <Time to search for in UTC>,
    'maxtimediff': <Time in minutes from 'timestamp' to search,
    'username': <Seach for data by this user>'
    }

    @return {} if no match. Otherwise:

    {
    '''

    try:
        query = request.get_json()
        check_pw = check_user_pw(query)

        # Maximum distance in meters for geotag query
        maxdist = -1.0

        ret = {}

        if 'sort' in query:
            sort_code = query['sort']
        else:
            sort_code = codes.SORT_NONE

        if check_pw is None:
            count, db_data = find_query(query, sort_code)

            if db_data is not None:
                ret["count"] = count
                ret["data"] = []
                for x in db_data:
                    if 'timestamp' in x:
                        x['timestamp'] = datetime.datetime.strftime(x['timestamp'],
                        codes.DATE_TIME_STR)

                        ret["data"].append(x)

                return make_json_response(ret, 200)
            else:
                return make_json_response({}, 200)
        else:
            return check_pw

    except Exception as e:
        raise
        return make_exception(e)


def init():
    global mydb, users, data
    client = MongoClient('mongodb://localhost:27017')
    mydb = client['sws3009']
    users = mydb['users']
    data = mydb['data']
    data.ensure_index([('loc', GEOSPHERE)])

    # Uncomment and use this if you want a server that
    # can be accessed remotely.

#    app.run(host = '0.0.0.0', port = '3007')

    # Use this if you want a server that is acessible only locally
    # RECOMMENDED. If you want this server to be accessed remotely
    # use a proxy like nginx to connect to it.

    app.run(port = '3007')

if __name__ == '__main__':
    init()
