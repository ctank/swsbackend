# SWS3009 Backend Toolkit #

The SWS Backend Toolkit ("the Toolkit") is a simple toolkit to help you develop and test your IoT application's communications with a backend cloud server, to store sensor and retrieve sensor data and readings.  You can search for sensor readings by date, by location or by who deposited it.

To help you ramp up on your data collection, you can access all data that has been stored on the server.  To help in your development, the Toolkit comprises of two parts:

- A local backend server that works the same way as the server on the backend cloud server. You can use this to test your code before connecting to the actual backend cloud server.
- A client library that lets you connect easily to the server to put and retrieve data.

### 1. Obtaining the SWS3009 Backend Toolkit

You can obtain the Toolkit by cloning this repository:

```
git clone https://bitbucket.org/ctank/swsbackend.git
```

Once you execute this command you will see the Toolkit in a folder called 'swsbackend'. **Change to this directory** and you will find the two main files that we need: backend.py and client.py:

- backend.py:  This is your local backend server.
- client.py: This is the client library that your code actually interacts with.

### 2. Setup Instructions

This section steps you through setting up your system to make use of the Toolkit. There are several important configurations that you need to make in order to use the Toolkit properly.

#### 2.1 Installation of Dependencies

The local backend server requires you to install MongoDB Community Edition, PyMongo, Flask and Requests:

1. Installing MongoDB Community Edition: See this [link](https://docs.mongodb.com/manual/installation/) for details. 

2. Installing PyMongo, Flask and Requests:

   Best practices dictate that you should never install any Python libraries outside of a contained environment, so the first thing that you need to do is to create one:

   ```
   python3 -m venv venv
   ```

   Now activate it. In Linux/MacOS:

   ```
   source ./venv/bin/activate
   ```

   In Windows:

   ```
   .\venv\scripts\activate.bat
   ```

   With your environment activated you are now ready to install PyMongo, Flask and Requests:

   `pip3 install pymongo flask requests`

#### 2.2 Configuring the Local Backend

There are several configuration options in codes.py that need to be set up if you do not want to use the default values:

File: codes.py

| Name of Constant | Default Value | Purpose                                                      |
| ---------------- | ------------- | ------------------------------------------------------------ |
| DATA_UPLOADS     | False         | Set to True to allow data to be uploaded to the server.      |
| UPLOAD_MIN_TIME  | 120           | Minimum time in seconds between uploads (2 minutes). Change this to 0 or 1 on your local backend. |
| MAX_UPLOAD_SIZE  | 524288        | Maximum data size in bytes for uploads (512 KB). Do not change this. |
| MAX_BINARY_SIZE  | 307200        | Maximum size of binary in bytes that you can attach (300 KB). Do not change this. |
| MAX_GPS_RADIUS   | 10000         | Maximum search radius in meters for GPS searches.            |
| MAX_TIME_DIFF    | 1440          | Maximum timeframe to search backwards for timestamp searches. |
| ALLOW_NEW_FIELDS | False         | Allows you to put arbitrary field names in your data. Do not change this. |
| FIELD_NAMES      | Various       | Standard backend database field names. If ALLOW_NEW_FIELDS is False, the backend will ignore field names that are not specified in this constant. Do not change this. |

You should change codes.MIN_UPLOAD_TIME to a much smaller value in your testing, The remaining constants should be left alone.

#### 2.3 Configuring the Client Library

There is only one constant to set and it is in uri.py:

uri.py:

| Name of Constant | Default Value           | Purpose                                                      |
| ---------------- | ----------------------- | ------------------------------------------------------------ |
| URI              | "http://127.0.0.1:3007" | Address of backend server. You will need to change this to "https://weather.sws3009.bid" to connect to the actual backend weather server, or "https://data.sws3009.bid" to connected to the backend cloud server for the mid-point evaluation project. |

### 3. Usage Instructions

This section covers how to use the local backend server and client libraries. In both cases you should start the Python environment if it hasn't already been started:

`source ./venv/bin.activate`

OR for Windows:

`.\venv\scripts\activate.bat`

#### 3.1 Using the Local Backend Server

**IMPORTANT: ENSURE THAT YOU HAVE INSTALLED MONGODB AND THAT IT IS RUNNING**

Usage of the local backend server is simple:

`python3 backend.py`

You will see the server start up:

![image-20210707102611344](image-20210707102611344.png)

Notice that the server never exits unless you interrupt it (e.g. with CTRL-C). Therefore it should run in its own window.

#### 3.2 Using the Client Library

This section walks you through a simple application for using the Client Library. You can find more details in the documentation [here](html/namespaceclient.html).

All functions except "create_user" require you to log in first using the "login" library call. The "create_user" call can only be used for the local backend server. You cannot create new users on the backend cloud server.

All calls except for "login" and "find_data" return a Requests object. If your response is stored in a variable called 'resp', you can access:

| Variable         | Meaning                                                      |
| ---------------- | ------------------------------------------------------------ |
| resp.status_code | Return code from browser. E.g. 200 is OK, 403 is Forbidden, 404 is Not Found,  etc. |
| resp.text        | The text returned by the server.                             |
| resp.json()      | JSON version of resp.text, assuming that the server sent a JSON object. |
| resp.headers     | The headers returned by the server, as a Pythong dict.       |

Please see this [link](https://docs.python-requests.org/en/master/) for more details.

##### 3.2.1 Importing the Library

This is straightforward:

`import client`

##### 3.2.2 Creating a New User

Before using your local backend server, you will first need to create a test user. Here we create a new user called 'sws3009' with the password 'mongobongo':

`resp = client.create_user(user='sws3009', password='mongobongo')`

**Response**

The 'resp' variable is a standard Response object. Please see above for how to use this object. You may receive 2 possible responses:

| Status Code | Text                                             | Meaning              |
| ----------- | ------------------------------------------------ | -------------------- |
| 200         | {'result': ERR_OK, 'details':'OK'}               | OK                   |
| 400         | {'result':ERR_DUP_USER, 'details':User exists.'} | User already exists. |

You can only create new users on your own local backend server. You will not be able to do so on the cloud backend server.

##### 3.3.3 Logging In

Aside from create_user, you must log in to use any other function in the Client library. Specify your username and password when logging in:

`client.login('sws3009', 'mongobongo')`

This function merely caches your username and password and does not actually connect to the backend server yet. This function does not return anything.

##### 3.3.4 Changing Password

You can use change_pw to change your password:

`client.change_pw('new_password')`

You must be logged in using client.login first. 

##### 3.3.5 Uploading Data

There are two functions provided for uploading data and both work in a similar way. Let's look at the first one. Let's suppose it is at the moment it is 31.5 degrees centigrade, 95% humidity, and a light level of 75. We are currently at longitude of 103.8198E and a latitude of 1.3521N. Let's also supposed we have a picture in the form of raw binary data in the variable called 'pix_data'. Then:

```resp = client.add_data(lon = 103.8198, lat = 1.3521, temp = 31.5, humid = 95, light = 75, raw = pix_data)```

Notice that longitude is specified first, then latitude. Also aside from longitude and latitude, *all* other parameters are optional. 

The second version of this function is similar except that it loads the binary data from a file. Suppose you have a picture in a file called 'mine.jpg', then:

```resp = client.add_data_file(lon = 103.8198, lat = 1.3521, temp = 31.5, humid = 95, light = 75, filename = 'mine.jpg')```

As before only lon and lat are required. All other parameters may be left out. E.g.:

```resp = client.add_data(lon = 103.8198, lat = 1.3521)```

**Response**

There are several possible responses:

| Status Code | Text                                                         | Meaning                                                      |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 200         | {'result': ERR_OK, 'details':'OK'}                           | OK                                                           |
| 400         | {'result':ERR_TOO_BIG, <br />'details':'Maximum upload size is XX bytes. You have YY bytes.'} | Size of data uploaded is too large. Reduce and try again.    |
| 400         | {'result':ERR_BAD_REQUEST, <br />'details':'Latitude or longitude missing'} | Your latitude or longitude data is missing.                  |
| 400         | {'result:'ERR_BAD_REQUEST, <br />'details':'No such user XX'} | You specified an invalid user during login. Use the login function to specify a valid user. |
| 400         | {'result':ERR_BAD_REQUEST,'details':'Bad current password'}  | You specified a valid user but invalid password. Use the login function to specify a valid password. |
| 400         | {'result':ERR_TOO_FAST,<br />'details':'Minimum time between uploads is XX seconds. Please wait another YY seconds before uploading again'} | You are uploading too frequently. Wait for the time specified in the message and try again. |

##### 3.3.6 Finding Data

The 'find_data' function lets you retrieve data not just from yourself, but all data that has been deposited on the cloud backend server. All parameters are optional, in which case all data from the server will be returned.

The parameter list is as follows:

| Parameter      | Purpose                                                      |
| -------------- | ------------------------------------------------------------ |
| lon            | Search for data at given the longitude and latitude. If lon is provided, then lat must also be provided. |
| lat            | Search for data at given the longitude and latitude. If lat is provided, then lon kust also be provided. |
| radius         | Maximum radius in meters to search from [lon, lat]. If not specified, but lat and lon are both specified, the backend will return data within 10,000 meters (10 km) of [lat, lon]. |
| timestamp      | Returns data recorded at time provided by timestamp. Timestamp must be of type datetime. All time specified must be in UTC (Coordinated Universal Time) and not local time. |
| interval_width | Maximum time range for timestamp search, in minutes. The backend will search for up to maxtimediff minutes before the timestamp specified. If not provided, the backend will search for up to 1,440 minutes (24 hours) before the timestamp provided. |
| username       | Search for data uploaded by user 'username'.                 |
| sort           | How to sort the return results. Import codes.py and use the following codes:<br />codes.SORT_NONE: Do not sort the results. This is the default.<br />codes.SORT_USERID_ASC: Sort by ID of user who uploaded, in ascending order.<br />codes.SORT_TIMESTAMP_ASC: Sort by timestamp, in ascending order.<br />codes.SORT_USERID_DESC: Sort by ID of user who uploaded, in descending order.<br />codes.SORT_TIMESTAMP_DESC: Sort by timestamp, in descending order. |

In the following example we search for data uploaded by user 'sws3009' between 14 June 2021 9.01 am UTC and 15 June 2021 9 am UTC, and sort the data in ascending order of timestamp:

```
import datetime
import codes

search_time = datetime.datetime(year=2021, month=6, day=15, hour=9, minute=0)
res = client.find_data(timestamp = search_time, username = 'sws3009', sort = codes.SORT_TIMESTAMP_ASC)

```

**Response**

Unlike the other functions, find_data returns the data found, and not a Python Response object. If no data is found, find_data returns:

`{}`

If data was found, find_data returns:

```{'count': <# of items found>,```

```'data':[{'username': <name of user who uploaded data>,```

```'timestamp':<Date and time of upload in Python datetime format>,```

```'temp': <Temperature reading>, 'humid':<Humidity reading>, 'light':<Light reading>,```

```'raw':<Raw data in binary>, 'loc':[<longitude>, <latitude>]}, {...}]}```

So 'count' contains the number of items found, while the items are listed in 'data' as an array.

##### 3.5.7 Example Code

This is a simple program that creates a new user called sws3009a, then randomly generates temperature and humidity data and stores it on the backend.

This code is provided as "example.py"

To run this code:

1. Open two windows.

2. Run the server in one window:

   `python3 server.py`

3. Run the example code in another window:

   `python3 example.py`

You will see the example program create a new user, enter a new piece of data, then download and print all the data.

Here is the code itself:

```python
'''! Example client code for depositing data on the backend server.
'''

import client
import datetime
import codes
import random

# My latitude and longitude
LON = 103.8198
LAT = 1.3521

# Username and password. Change this to the user/password pair given to you.

USER = 'sws15'
PASSWORD = 'manggocat'

def print_result(result):
    '''! Prints the result from the server
    '''

    if result.status_code == 200:
        print("Operation OK.\n")
    else:
        outcome = result.json()
        print("Error code: %d, details: %s\n" % (outcome['result'],
        outcome['details']))

def print_data(data):
    '''! Prints data returned by the server.
    '''

    if data == {}:
        print("No data returned.\n")
        return

    print()
    print("Data from server:")
    print("-----------------")
    print()

    print("# of items found; %d\n" % data['count'])

    for datum in data['data']:
        print("Location in [lon, lat]: [%f, %f]." % (datum['loc'][0],
        datum['loc'][1]))
        print("Time in dd/mm/yy HH:MM:SS: ",
        datetime.datetime.strftime(datum['timestamp'],
        '%d/%m/%y %H:%M:%S'))
        print()
        print("Temperature: ", datum['temp'])
        print("Humidity: ", datum['humid'])
        print("Light: ", datum['light'])
        print()
        print("Size of binary attachment: ", len(datum['raw']))

        print("\n-------------------------------------------------\n")

def main():

    # Create a new user. We will get an error message if the user exists.
    print("\nAdding new user.")
    resp = client.create_user(USER, PASSWORD)
    print_result(resp)
    print()

    # Log in:
    client.login(USER, PASSWORD)

    # Enter random temperature and humidity data
    temp = random.random() * 33.5
    humid = random.random() * 100
    print("Adding new data.")
    resp = client.add_data(lon = LON, lat = LAT, temp = temp, humid = humid)
    print_result(resp)
    print()

    #Pull the data we have
    print("Searching for data:")
    data  = client.find_data(username = 'sws3009a', sort =
    codes.SORT_TIMESTAMP_DESC)

    # If we have a valid data response, print it.
    if 'count' in data:
        print_data(data)
    else:
        print_result(data)

if __name__ == '__main__':
    main()
```
