''' Contains all the return codes for the backend
'''

## 
# Error codes
ERR_OK = 0			    # OK
ERR_DUP_USER = 1		# Duplicate user
ERR_BAD_USER = 2		# No such user
ERR_BAD_PW = 3			# Bad password
ERR_TOO_FAST = 4		# Insufficient time between adding new data
ERR_TOO_MANY = 5		# Too many add data requests made
ERR_TOO_BIG = 6			# Data added is too large
ERR_BAD_REQUEST = 7     # Malformed request
ERR_EMPTY = 8           # No data to insert
ERR_READ_ONLY = 9       # Server set to read-only mode, cannot upload
ERR_UNKNOWN = 255		# Unknown error

##
# Set this to TRUE to allow data uploads
DATA_UPLOADS = False

## 
# Limits
UPLOAD_MIN_TIME = 0     # Minimum seconds between uploads
MAX_UPLOAD_SIZE = 524288 # Maximum upload size in bytes, nominally 512KB
MAX_BINARY_SIZE = 307200 # Maximum size for binary attachments, nominally 300KB

## 
# Query sort codes
SORT_NONE           = 0     # Do not sort results.
SORT_USERID_ASC     = 1     # Sort by user, ascending order.
SORT_TIMESTAMP_ASC  = 2     # Sort by timestamp, ascending order.
SORT_USERID_DESC    = 3     # Sort by user, descending order.
SORT_TIMESTAMP_DESC = 4     # Sort by timestamp, descending order.

##
# Standard field 
FIELD_NAMES = ['timestamp', 'temp', 'humid', 'light', 'rain', 'raw']

##
# If true, allow adding non-standard fields
ALLOW_NEW_FIELDS = False

## 
# Maximum GPS radius in meters
MAX_GPS_RADIUS = 10000

##
# Default timeframe to search is -1440 minutes (24 hours)
MAX_TIME_DIFF = 1440

## 
# Datetime conversion string
DATE_TIME_STR = '%d:%m:%Y:%H:%M:%S'
