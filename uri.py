''' uri.py - Contains the URI for the backend server.
'''

##
# URI to of backend server.
# Need to change this to https://data.sws3009.bid:3009 to work with
# the actual backend server
#

# Uncomment for local development
URI = "http://127.0.0.1:3007"

# Uncomment for actual backend
#URI = "https://data.sws3009.bid:3009"
