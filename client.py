"""! @brief Client library for accessing the SWS3009 Base System Backend. """

# imports

import requests
import codes
import uri
import json
import datetime
import base64

headers = {'Content-type':'application/json'}

userid = ''
passwd = ''

##
# Print response messages
#

def print_resp(resp):

    try:
        result = resp.json()
        print("Response code: %d. Result: %d. Result Details: %s." %
        (resp.status_code, result['result'], result['details']))
    except Exception as e:
        print("Error printing response: ", e)

##
# @section: Helper functions
# This part of the library contains internal functions used later on.
#

def send_request(endpoint, request):
    """! Forms JSON POST requests to the backend.

    @param endpoint The endpoint to send to, e.g. '/user/add'
    @param request  The POST data in the form of a Python dictionary.

    @return The result from the server

    """

    return requests.post(uri.URI + endpoint, headers=headers,
    data = json.dumps(request))

# User related calls

def login(user, password):
    '''! Records the user name and password in a global variable so that we
    don't need to include it in all calls.

    @param user User name
    @param password Password
    
    '''

    global userid, passwd

    userid = user
    passwd = password

def create_user(user, password):
    """! Create a new user.

    @param user New user ID.
    @param password Password for the new user.

    @return The result from the server.

    The result string can be obtained from result.text
    """

    req  = {'userid':user, 'passwd':password}
    return send_request('/user/newuser', req)

def change_pw(new_password):
    """! Change existing user's password.

    @param new_password New password to change to.

    @return Result from server.
    """

    global userid, passwd

    req = {'userid':userid, 'passwd':passwd, 'new_passwd':new_password}
    return send_request('/user/newpassword', req)

## 
# Helper functions for data related calls

def load_binary_file(filename):
    ''' Load up a binary file.

    @param filename Name of file to load

    @return Binary data from file.

    '''

    try:
        with open(filename, "rb") as f:
            file_data = f.read()
    except:
        print("Unable to open file.")
        file_data = None

    return file_data

def add_data_file(lon, lat, timestamp = None, temp=-1.0, humid=-1.0,
light=-1.0, rain = -1.0, filename = None):
    ''' Add data to backend, optionlly reading binary from a file.

    Except for lon and lat, all other parameters are optional.

    @param lon, lat  Longitude and latitude
    @param temp     Temperature
    @param humid    Humitiy
    @param light    Light level
    @param filename Name of binary file to load

    @return Result from server.
    
    '''

    global userid, passwd

    if filename is None:
        add_data(lon, lat, timestamp, temp, humid, light, rain)
    else:
        file_data = load_binary_file(filename)
        
        return add_data(lon, lat, timestamp, temp, humid, light, rain, file_data)

    
def add_data(lon, lat, timestamp=None, temp=-1.0, humid=-1.0, light=-1.0, rain = -1.0,
raw=None):
    """! Add new IoT sensor data to the database.

    @param lat  Current location's latitude
    @param lon  Current location's longitude
    @param timestamp Date/time in DD:MM:YYYY:HH:MM:SS format
    @param temp Temperature (optional)
    @param humid    Humidity (optional)
    @param light    Light level (optional)
    @param rain     Whether it is raining
    @param raw  Raw data (optional). 

    @returns    Response from server
    """

    global userid, passwd

    if timestamp is None:
        thetime = datetime.datetime.utcnow()

        timestr = datetime.datetime.strftime(thetime, codes.DATE_TIME_STR)
    else:
        timestr = timestamp

    req = {'userid':userid, 'passwd':passwd, 'lat':lat, 'lon':lon,
    'timestamp':timestr, 'temp':temp, 'humid':humid, 
    'light':light, 'rain':rain}

    if raw is None:
        req['raw'] = ''
    else:
        try:
            if len(raw) <= codes.MAX_BINARY_SIZE:
                req['raw'] = base64.b64encode(raw).decode('utf-8')
            else:
                print("Raw data too big. Ignored.")
                req['raw'] = ''
        except:
            print("Unable to encode raw data.")
            req['raw'] = ''

    if len(req) > codes.MAX_UPLOAD_SIZE:
        print("Upload size too large. Exiting.")
        return None
    else:
        return send_request('/data/add', req)

def find_data(lon = None, lat = None, radius = None,
timestamp = None, interval_width = None, username = None,
sort = codes.SORT_NONE):
    """! Search for data. All parameters are optional.

    @param lat  Latitude for geo search. Lat and Lon must be specified
    together.
    @param lon  Longitude for geo search. Lat and Lon must be specified
    together.
    @param radius   Maximum search radius in meters. Optional.
    @param timestamp    Timestamp search 
    @param interval_width   Timestamp search radius in minutes. Optional.
    @param username Search for data for a particular user.
    @param sort - Sort by user ID or timestamp, ascending or descending.

    @returns    Data from server in this format:
    {'count':<# of records found>,
     'data': {
        'username':<Name of user who deposited record>,
        'timestamp':<Time stamp of record as datetime.datetime object>,
        'temp': <Temperature>,
        'humid': <Humidity>,
        'light': <Light level>,
        'temp': <Temperature>,
        'rain':<Rain: 0.1 = No, 0.9 = Yes>,
        'raw': <Binary data>,
        'loc': <Location in [longitude, latitude]>}

    """

    global userid, passwd

    req = {"userid":userid, "passwd":passwd, 'sort':sort}

    if lat is not None:
        if lon is not None:
            req['lat'] = lat;
            req['lon'] = lon;

            if radius is not None:
                req['maxdist'] = radius
        else:
            print("Longitude must be specified.")
            return None
    elif lon is not None:
        print("Latitude must be specified.")
        return None

    if timestamp is not None:
        if type(timestamp) is not datetime.datetime:
            print("Timestamp must be of type datetime.")
            return None

        req['timestamp'] = datetime.datetime.strftime(timestamp, codes.DATE_TIME_STR)
        if interval_width is not None:
            req['maxtimediff'] = interval_width

    if username is not None:
        req['username'] = username
    
    resp = send_request('/data/search', req)

    resp_dict = json.loads(resp.text)

    if resp_dict != {} and 'data' in resp_dict:

        for x in resp_dict['data']:
            if 'timestamp' in x:
                x['timestamp'] = datetime.datetime.strptime(
                x['timestamp'], codes.DATE_TIME_STR)

            if x['raw'] != '':
                try:
                    x['raw'] = base64.b64decode(x['raw'].encode('utf-8'))
                except:
                    raise
                    print("Error, unable to process raw data.")

    return resp_dict


    
    

