'''! Example client code for depositing data on the backend server.
'''

import client
import datetime
import codes
import random

# My latitude and longitude
LON = 103.8198
LAT = 1.3521

# Username and password
USER = 'sws3009a'
PASSWORD = 'mongobongo'

def print_result(result):
    '''! Prints the result from the server
    '''

    if result.status_code == 200:
        print("Operation OK.\n")
    elif result.status_code == 500:
        print("Technical error.")
    else:
        outcome = result.json()
        print("Error code: %d, details: %s\n" % (outcome['result'],
        outcome['details']))

def print_data(data):
    '''! Prints data returned by the server.
    '''

    if data == {}:
        print("No data returned.\n")
        return

    print()
    print("Data from server:")
    print("-----------------")
    print()

    print("# of items found; %d\n" % data['count'])

    for datum in data['data']:
        print("Location in [lon, lat]: [%f, %f]." % (datum['loc'][0],
        datum['loc'][1]))
        print("Time in dd/mm/yy HH:MM:SS: ",
        datetime.datetime.strftime(datum['timestamp'],
        '%d/%m/%y %H:%M:%S'))
        print()
        print("Temperature: ", datum['temp'])
        print("Humidity: ", datum['humid'])
        print("Light: ", datum['light'])
        print()
        print("Size of binary attachment: ", len(datum['raw']))

        print("\n-------------------------------------------------\n")

def main():

    # Create a new user. We will get an error message if the user exists.
    print("\nAdding new user.")
    resp = client.create_user(USER, PASSWORD)
    print_result(resp)
    print()

    # Log in:
    client.login(USER, PASSWORD)

    # Enter random temperature and humidity data
    temp = random.random() * 33.5
    humid = random.random() * 100
    print("Adding new data.")
    resp = client.add_data(lon = LON, lat = LAT, temp = temp, humid = humid)
    print_result(resp)
    print()

    #Pull the data we have
    print("Searching for data:")
    data  = client.find_data(username = 'sws3009a', sort =
    codes.SORT_TIMESTAMP_DESC)

    # If we have a valid data response, print it.
    if 'count' in data:
        print_data(data)
    else:
        print_result(data)

if __name__ == '__main__':
    main()
